# Examen Applications Entreprises Multi-Tiers
Groupe B1:
- Giorgio Caculli LA196672
- Benoit Ferreira Rodrigues LA199395
- Guillaume Lambert
- Tanguy Taminiau

## SQL
1. CREATE DATABASE dbb1;
2. CREATE USER 'gb1'@'%' IDENTIFIED BY '4712';
3. GRANT ALL PRIVILEGES ON dbb1.* TO 'gb1'@'%' WITH GRANT OPTION;
4. FLUSH PRIVILEGES;
5. QUIT;
