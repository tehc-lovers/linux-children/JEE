package be.helha.aemt.ejb;

import be.helha.aemt.dao.PictureDAO;
import be.helha.aemt.entities.Picture;
import org.primefaces.model.file.UploadedFile;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Stateless
@LocalBean
public class PictureEJB implements Serializable
{

    @EJB
    private PictureDAO pictureDAO;

    public PictureEJB()
    {

    }

    public Picture getImageByName( String name )
    {
        return pictureDAO.findImagesByName( name );
    }

    public List< Picture > getAllImages()
    {
        return pictureDAO.findAll();
    }

    public Picture getImageById( int id )
    {
        return pictureDAO.read( id );
    }

    public Picture create( String path )
    {
        File file = new File( path );
        BufferedImage buff;
        ByteArrayOutputStream bos;
        try
        {
            buff = ImageIO.read( file );
            bos = new ByteArrayOutputStream();
            ImageIO.write( buff, "jpg", bos );
            Picture im = new Picture( path, bos.toByteArray() );
            return pictureDAO.create( im );
        } catch ( IOException e )
        {
            e.printStackTrace();
        }

        return null;
    }

}
