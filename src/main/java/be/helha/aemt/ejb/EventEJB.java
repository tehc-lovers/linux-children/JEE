package be.helha.aemt.ejb;

import be.helha.aemt.dao.EventDAO;
import be.helha.aemt.entities.Event;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.time.LocalDateTime;
import java.util.List;

@Stateless
@LocalBean
public class EventEJB
{
    @EJB
    private EventDAO eventDAO;

    public EventEJB()
    {
    }

    public List< Event > getAllEvents()
    {
        return eventDAO.findAll();
    }

    public Event createEvent( String nom, String place, String description, int dureeTot )
    {
        return eventDAO.create( new Event( nom, place, description, dureeTot ) );
    }

    public Event getEventById( int id )
    {
        return eventDAO.read( id );
    }

    public Event updateEvent( Integer id, Event newEvent )
    {
        Event oldEvent = eventDAO.read( id );
        return eventDAO.update( oldEvent, newEvent );
    }
}
