package be.helha.aemt.ejb;

import be.helha.aemt.dao.ActivityDAO;
import be.helha.aemt.dao.LessonDAO;
import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.Lesson;
import org.jline.builtins.Less;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

@Stateless
@LocalBean
public class LessonEJB
{
    @EJB
    private LessonDAO lessonDAO;
    @EJB
    private ActivityDAO activityDAO;

    public LessonEJB()
    {
    }

    public List< Lesson > getAllLessons()
    {
        return lessonDAO.findAll();
    }

    public Lesson getLessonById( int idActivity, int idLesson )
    {
        List< Lesson > lessons = getAllLessons();
        for( Lesson lesson : lessons )
        {
            if( lesson.getId() == idLesson )
            {
                return lesson;
            }
        }
        return null;
    }

    public Lesson createLesson( Integer idActivity, String name, Date startingDateTimeUnformatted, int duration )
    {
        Activity activity = activityDAO.read( idActivity );
        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm" );
        LocalDateTime startingDateTime = LocalDateTime.parse( startingDateTimeStr, formatter );*/
        LocalDateTime startingDateTime = startingDateTimeUnformatted.toInstant().atZone( ZoneId.systemDefault() ).toLocalDateTime();
        Lesson lesson = lessonDAO.create( new Lesson( name, startingDateTime, duration ) );
        List< Lesson > activityLessons = new ArrayList<>();
        for( Lesson l : activity.getLessons() )
        {
            if( activityLessons.contains( l ) )
            {
                continue;
            }
            activityLessons.add( l );
        }
        if( activityLessons.contains( lessonDAO.read( lesson.getId() ) ) )
        {
            return null;
        }
        activityLessons.add( lesson );
        activity.setLessons( activityLessons );
        activityDAO.update( activityDAO.read( idActivity ), activity );
        return lesson;
    }

    public Lesson getLessonById( int id )
    {
        return lessonDAO.read( id );
    }

    public List< Lesson> getAllLessonsByActivity( int idActivity )
    {
        return lessonDAO.getAllLessonsByActivityId( idActivity );
    }

    public Lesson getLessonByName( String name )
    {
        return lessonDAO.findLessonByName( name );
    }
}
