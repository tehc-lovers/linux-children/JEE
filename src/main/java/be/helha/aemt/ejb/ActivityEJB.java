package be.helha.aemt.ejb;

import be.helha.aemt.dao.ActivityDAO;
import be.helha.aemt.dao.CourseDAO;
import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.ActivitySubscribable;
import be.helha.aemt.entities.Course;
import be.helha.aemt.entities.Lesson;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
@LocalBean
public class ActivityEJB
{
    @EJB
    private ActivityDAO activityDAO;
    @EJB
    private CourseDAO courseDao;
    
    

    public ActivityEJB()
    {
    }

    public List< Activity > getAllActivities()
    {
        return activityDAO.findAll();
    }

    public Activity getActivityById( int id )
    {
        return activityDAO.read( id );
    }
    
    public Course createCourse(Course c) {
    	return courseDao.create(c);
    }

    public List< ActivitySubscribable > getAllActivitiesByUserId( Integer id )
    {
        return activityDAO.findAllActivitiesByUserId( id );
    }
}
