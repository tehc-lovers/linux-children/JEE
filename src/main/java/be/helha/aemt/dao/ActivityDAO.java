package be.helha.aemt.dao;

import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.ActivitySubscribable;
import be.helha.aemt.entities.Lesson;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.io.Serializable;
import java.util.List;

@Stateless
@LocalBean
public class ActivityDAO extends DAO< Activity >  implements Serializable
{
    public Activity findActivityByName( Activity a )
    {
        if ( a == null || a.getName() == null )
        {
            return null;
        }
        String queryString = "SELECT a FROM Activity a WHERE a.name = :name";
        Query query = em.createQuery( queryString );
        query.setParameter( "name", a.getName() );
        Activity resultPublication = null;
        try
        {
            resultPublication = ( Activity ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return resultPublication;
    }

    @Override
    public Activity create( Activity activity )
    {
    	if ( findActivityByName( activity ) != null )
        {
            return null;
        }
        if ( activity == null )
        {
            return null;
        }
        if ( activity.getName() == null )
        {
            return null;
        }
        em.merge( activity );
        return activity;
    }

    @Override
    public Activity read( Integer id )
    {
        if ( id == null )
        {
            return null;
        }
        Activity publication = em.find( Activity.class, id );
        if ( publication != null )
        {
            /* TODO: Log? Maybe unnecessary... */
        }
        return publication;
    }

    @Override
    public Activity update( Activity object1, Activity object2 )
    {
        if ( object1 == null || object2 == null || object1.getId() == null )
        {
            return null;
        }

        //Gérer une exeption ici

        Activity findActivity = em.find( Activity.class, object1.getId() );

        if ( findActivity == null )
        {
            return null;
        }

        findActivity.setLessons( object2.getLessons() );

        em.merge( findActivity );
        return findActivity;
    }

    @Override
    public boolean delete( Activity object )
    {
        if ( object == null || object.getName() == null )
            return false;

        try
        {
            object = em.find( Activity.class, object );
            em.remove( object );
        } catch ( Exception e )
        {
            e.getMessage();
        }

        return true;
    }

    @Override
    public List< Activity > findAll()
    {
        String queryString = "SELECT a FROM Activity a";
        TypedQuery< Activity > query = em.createQuery( queryString, Activity.class );
        return query.getResultList();
    }

    public List< ActivitySubscribable > findAllActivitiesByUserId( Integer idUser )
    {
        String queryString = "SELECT u.activities FROM User u WHERE u.id = :id";
        TypedQuery< ActivitySubscribable > query = em.createQuery( queryString, ActivitySubscribable.class );
        query.setParameter( "id", idUser );
        return query.getResultList();
    }
}
