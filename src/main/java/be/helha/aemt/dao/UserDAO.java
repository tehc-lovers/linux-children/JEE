package be.helha.aemt.dao;

import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.ActivitySubscribable;
import be.helha.aemt.entities.Course;
import be.helha.aemt.entities.User;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

@Stateless
@LocalBean
public class UserDAO extends DAO< User >
{
    @EJB
    ActivityDAO activityDAO;

    public User findUserByLogin( User u )
    {
        if ( u == null || u.getLogin() == null )
        {
            return null;
        }
        String loginQuery = "SELECT u FROM User u WHERE u.login = :login";
        Query query = em.createQuery( loginQuery );
        query.setParameter( "login", u.getLogin() );
        User resultUser = null;
        try
        {
            resultUser = ( User ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return resultUser;
    }

    public User findUserByLogin( String login )
    {
        if ( login == null )
        {
            return null;
        }
        String loginQuery = "SELECT u FROM User u WHERE u.login = :login";
        Query query = em.createQuery( loginQuery );
        query.setParameter( "login", login );
        User resultUser = null;
        try
        {
            resultUser = ( User ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return resultUser;
    }

    public boolean subscribeActivity( Integer idUser, Integer idActivity )
    {
        User user = read( idUser );
        Activity a = activityDAO.read( idActivity );
        if( activityDAO.read( idActivity ) instanceof ActivitySubscribable )
        {
            Course c = ( Course ) a;
            Course course = new Course();
            course.setId( a.getId() );
            course.setName( a.getName() );
            course.setPlace( a.getPlace() );
            course.setDureeTot( a.getDureeTot() );
            course.setPlacesMax( c.getPlacesMax() );
            course.setDescription( a.getDescription() );
            course.setCourseType( c.getCourseType() );
            course.setRequirements( c.getRequirements() );

            if( user.subscribe( course ) )
            {
                em.merge( user );
                return true;
            }
        }
        return false;
    }

    public boolean unsubscribeActivity( Integer idUser, Integer idActivity )
    {
        try
        {
            User user = read( idUser );
            Activity a = activityDAO.read( idActivity );
            if( a instanceof ActivitySubscribable )
            {
                Course c = ( Course ) a;
                if( user.unsubscribe( c ) )
                {
                    em.merge( user );
                    return true;
                }
                return false;
            }
            return false;
        } catch ( Exception e )
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public User create( User user )
    {
        if ( findUserByLogin( user ) != null )
        {
            return null;
        }
        if ( user == null )
        {
            return null;
        }
        if ( user.getLogin() == null )
        {
            return null;
        }
        try
        {
            String passwordBase64 = new String( Base64.getEncoder().encode(
                    MessageDigest.getInstance( "SHA-256" )
                            .digest( user.getPassword().getBytes(
                                    StandardCharsets.UTF_8 ) ) ) );
            user.setPassword( passwordBase64 );
            em.persist( user );
            return user;
        } catch ( Exception e )
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User read( Integer id )
    {
        if ( id == null )
        {
            return null;
        }
        User utilisateur = em.find( User.class, id );
        if ( utilisateur != null )
        {
            /* TODO: Log? Maybe unnecessary... */
        }
        return utilisateur;
    }

    @Override
    public User update( User object1, User object2 )
    {

        if ( object1 == null || object2 == null || object1.getId() == null )
        {
            return null;
        }

        //Gérer une exeption ici

        User findUser1 = em.find( User.class, object1.getId() );

        if ( findUser1 == null )
        {
            return null;
        }

        findUser1.setLogin( object2.getLogin() );

        String passwordBase64;
        try
        {
            passwordBase64 = new String( Base64.getEncoder().encode(
                    MessageDigest.getInstance( "SHA-256" )
                            .digest( object2.getPassword().getBytes(
                                    StandardCharsets.UTF_8 ) ) ) );
            findUser1.setPassword( passwordBase64 );

        } catch ( NoSuchAlgorithmException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //findUser1.setRole( object2.getRole() );

        em.merge( findUser1 );
        return findUser1;

    }

    @Override
    public boolean delete( User object )
    {
        if ( object == null || object.getLogin() == null )
        {
            return false;
        }

        object = em.find( User.class, object.getId() );

        em.remove( object );
        return true;
    }

    @Override
    public List< User > findAll()
    {
        String loginQuery = "SELECT u FROM User u";
        TypedQuery< User > query = em.createQuery( loginQuery, User.class );
        return query.getResultList();
    }

    public List< ActivitySubscribable > findAllActivitiesByUserId( Integer idUser )
    {
        return activityDAO.findAllActivitiesByUserId( idUser );
    }
}
