package be.helha.aemt.dao;

import be.helha.aemt.entities.Course;
import be.helha.aemt.entities.Event;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.util.List;

@Stateless
@LocalBean
public class CourseDAO extends DAO< Course >
{
    public CourseDAO()
    {
        super();
    }
    
    public Course findEventByName( Course e)
    {
        if ( e == null || e.getName() == null )
        {
            return null;
        }
        String queryString = "SELECT e FROM Course e WHERE e.name = :name";
        Query query = em.createQuery( queryString );
        query.setParameter( "name", e.getName() );
        Course result = null;
        try
        {
            result = ( Course ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return result;
    }

    @Override
    public Course create( Course object )
    {
    	if ( findEventByName( object ) != null )
        {
            return null;
        }
        if ( object == null )
        {
            return null;
        }
        if ( object.getName() == null )
        {
            return null;
        }
        em.merge( object );
        return object;
    }

    @Override
    public Course read( Integer id )
    {
    	if ( id == null )
        {
            return null;
        }
        Course event = em.find( Course.class, id );
        if ( event != null )
        {
            /* TODO: Log? Maybe unnecessary... */
        }
        return event;
    }

    @Override
    public Course update( Course object1, Course object2 )
    {
    	if ( object1 == null || object2 == null || object1.getId() == null )
        {
            return null;
        }

        //Gérer une exeption ici

        Course findEvent1 = em.find( Course.class, object1.getId() );

        if ( findEvent1 == null )
        {
            return null;
        }

        findEvent1.setLessons( object2.getLessons() );

        em.merge( findEvent1 );
        return findEvent1;
    }

    @Override
    public boolean delete( Course object )
    {
    	if ( object == null || object.getName() == null )
        {
            return false;
        }

        object = em.find( Course.class, object.getId() );

        em.remove( object );
        return true;
    }

    @Override
    public List< Course > findAll()
    {
    	String nameQuery = "SELECT e FROM Course e";
        TypedQuery< Course > query = em.createQuery( nameQuery, Course.class );
        return query.getResultList();
    }
}
