package be.helha.aemt.dao;

import be.helha.aemt.entities.Lesson;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
@LocalBean
public class LessonDAO extends DAO< Lesson >
{
    public LessonDAO()
    {
        super();
    }

    public Lesson findLessonByName( Lesson l )
    {
        if ( l == null || l.getName() == null )
        {
            return null;
        }
        String queryString = "SELECT l FROM Lesson l WHERE l.name = :name";
        Query query = em.createQuery( queryString );
        query.setParameter( "name", l.getName() );
        Lesson result = null;
        try
        {
            result = ( Lesson ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return result;
    }

    public Lesson findLessonByName( String name )
    {
        if ( name == null )
        {
            return null;
        }
        String queryString = "SELECT l FROM Lesson l WHERE l.name = :name";
        Query query = em.createQuery( queryString );
        query.setParameter( "name", name );
        Lesson result = null;
        try
        {
            result = ( Lesson ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return result;
    }
    
    @Override
    public Lesson create( Lesson object )
    {
    	
    	if ( findLessonByName( object ) != null )
        {
            return null;
        }
        if ( object == null )
        {
            return null;
        }
        if ( object.getName() == null )
        {
            return null;
        }
        em.merge( object );
        return object;
    }

    @Override
    public Lesson read( Integer id )
    {
        if ( id == null )
        {
            return null;
        }
        Lesson publication = em.find( Lesson.class, id );
        if ( publication != null )
        {
            /* TODO: Log? Maybe unnecessary... */
        }
        return publication;
    }

    @Override
    public Lesson update( Lesson object1, Lesson object2 )
    {
        if ( object1 == null || object2 == null || object1.getId() == null )
        {
            return null;
        }

        //Gérer une exeption ici

        Lesson findLesson = em.find( Lesson.class, object1.getId() );

        if ( findLesson == null )
        {
            return null;
        }

        em.merge( findLesson );
        return findLesson;
    }

    @Override
    public boolean delete( Lesson object )
    {
        if ( object == null || object.getName() == null )
            return false;

        try
        {
            object = em.find( Lesson.class, object );
            em.remove( object );
        } catch ( Exception e )
        {
            e.getMessage();
        }

        return true;
    }

    @Override
    public List< Lesson > findAll()
    {
        String queryString = "SELECT l FROM Lesson l";
        TypedQuery< Lesson > query = em.createQuery( queryString, Lesson.class );
        return query.getResultList();
    }

    public List< Lesson> getAllLessonsByActivityId( int idActivity )
    {
        String queryString = "SELECT a.lessons FROM Activity a WHERE a.id = :id";
        TypedQuery< Lesson > query = em.createQuery( queryString, Lesson.class );
        query.setParameter( "id", idActivity );
        return query.getResultList();
    }
}
