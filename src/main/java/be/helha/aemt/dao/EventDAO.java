package be.helha.aemt.dao;
import be.helha.aemt.entities.Event;
import be.helha.aemt.entities.Event;
import be.helha.aemt.entities.Event;
import be.helha.aemt.entities.Event;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

@Stateless
@LocalBean
public class EventDAO extends DAO< Event >
{
    public EventDAO()
    {
        super();
    }

    public Event findEventByName( Event e)
    {
        if ( e == null || e.getName() == null )
        {
            return null;
        }
        String queryString = "SELECT e FROM Event e WHERE e.name = :name";
        Query query = em.createQuery( queryString );
        query.setParameter( "name", e.getName() );
        Event result = null;
        try
        {
            result = ( Event ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }
        return result;
    }
    
    @Override
    public Event create( Event object )
    {
    	
    	if ( findEventByName( object ) != null )
        {
            return null;
        }
        if ( object == null )
        {
            return null;
        }
        if ( object.getName() == null )
        {
            return null;
        }
        em.merge( object );
        return object;
    }

    @Override
    public Event read( Integer id )
    {
        if ( id == null )
        {
            return null;
        }
        Event event = em.find( Event.class, id );
        if ( event != null )
        {
            /* TODO: Log? Maybe unnecessary... */
        }
        return event;
    }

    @Override
    public Event update( Event object1, Event object2 )
    {
        if ( object1 == null || object2 == null || object1.getId() == null )
        {
            return null;
        }

        //Gérer une exeption ici

        Event findEvent1 = em.find( Event.class, object1.getId() );

        if ( findEvent1 == null )
        {
            return null;
        }

        findEvent1.setLessons( object2.getLessons() );

        em.merge( findEvent1 );
        return findEvent1;
    }

    @Override
    public boolean delete( Event object )
    {
        if ( object == null || object.getName() == null )
        {
            return false;
        }

        object = em.find( Event.class, object.getId() );

        em.remove( object );
        return true;
    }

    @Override
    public List< Event > findAll()
    {
        String nameQuery = "SELECT e FROM Event e";
        TypedQuery< Event > query = em.createQuery( nameQuery, Event.class );
        return query.getResultList();
    }
}
