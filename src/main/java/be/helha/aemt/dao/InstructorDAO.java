package be.helha.aemt.dao;

import be.helha.aemt.entities.Instructor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class InstructorDAO extends DAO< Instructor >
{
    public InstructorDAO()
    {
        super();
    }

    @Override
    public Instructor create( Instructor object )
    {
        return super.create( object );
    }

    @Override
    public Instructor read( Integer id )
    {
        return super.read( id );
    }

    @Override
    public Instructor update( Instructor object1, Instructor object2 )
    {
        return super.update( object1, object2 );
    }

    @Override
    public boolean delete( Instructor object )
    {
        return super.delete( object );
    }

    @Override
    public List< Instructor > findAll()
    {
        return super.findAll();
    }
}
