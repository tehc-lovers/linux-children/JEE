package be.helha.aemt.dao;

import be.helha.aemt.entities.Picture;
import jakarta.persistence.NoResultException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;

@Stateless
@LocalBean
public class PictureDAO extends DAO< Picture > implements Serializable
{

    public PictureDAO()
    {
        super();
    }

    public Picture findImagesByName( String name )
    {
        if ( name == null )
            return null;
        String queryS = "SELECT p FROM Picture p WHERE p.name=?1";
        Query query = em.createQuery( queryS );
        query.setParameter( 1, name );
        Picture result = null;
        try
        {
            result = ( Picture ) query.getSingleResult();
        } catch ( NoResultException nre )
        {
            nre.printStackTrace();
        }

        return result;
    }

    @Override
    public Picture create( Picture im )
    {
        if ( im == null )
            return null;
        em.merge( im );
        return im;
    }

    @Override
    public Picture read( Integer id )
    {
        if ( id == null )
            return null;
        Picture im = em.find( Picture.class, id );
        return im;
    }

    @Override
    public List< Picture > findAll()
    {
        String queryS = "SELECT p FROM Picture p";
        TypedQuery< Picture > query = em.createQuery( queryS, Picture.class );
        return query.getResultList();
    }

}
