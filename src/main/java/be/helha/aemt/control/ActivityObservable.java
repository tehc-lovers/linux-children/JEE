package be.helha.aemt.control;

import java.util.ArrayList;
import java.util.List;

public abstract class ActivityObservable
{
    private final List< Observer > observers = new ArrayList<>();

    public void addObserver( Observer o )
    {
        if ( !observers.contains( o ) )
            observers.add( o );
    }

    public void removeObserver( Observer o )
    {
        observers.remove( o );
    }

    public void notifyObservers()
    {
        List< Observer > copie = new ArrayList<>();
        copie.addAll( observers );

        for ( Observer o : copie )
        {
            o.actualise( this );
        }
    }
}
