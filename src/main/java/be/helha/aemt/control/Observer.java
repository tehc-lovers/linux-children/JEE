package be.helha.aemt.control;

public interface Observer {
    public void actualise(ActivityObservable o);
}
