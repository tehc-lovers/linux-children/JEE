package be.helha.aemt.control;

import be.helha.aemt.ejb.ActivityEJB;
import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.ActivitySubscribable;
import be.helha.aemt.entities.Lesson;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class ActivityControl implements Serializable
{
    @EJB
    private ActivityEJB activityEJB;
    private Activity activity;
    private Lesson lesson;

    public String showActivitiesListPage()
    {
        return "/activity/activity_list.xhtml?faces-redirect=true";
    }

    public String showActivityDetailsPageByActivityId( Integer id )
    {
        return "/activity/activity_details.xhtml?facets-redirect=true&id_activity=" + id;
    }

    public String showCreateActivityPage()
    {
        return "/admin/activity_create.xhtml?faces-redirect=true";
    }

    public List< Activity > showActivityList()
    {
        return activityEJB.getAllActivities();
    }

    public List< ActivitySubscribable > showActivityByUserId( Integer id )
    {
        return activityEJB.getAllActivitiesByUserId( id );
    }

    public Activity getActivityById( int id )
    {
        activity = activityEJB.getActivityById( id );
        return activity;
    }

    public Activity getActivity()
    {
        return activity;
    }

    public void setActivity( Activity activity )
    {
        this.activity = activity;
    }
}
