package be.helha.aemt.control;

import be.helha.aemt.ejb.PictureEJB;
import be.helha.aemt.entities.Picture;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

@Named
@SessionScoped
public class PictureController implements Serializable
{

    @EJB
    private PictureEJB pictureEJB;

    private String picturePath;

    public Picture create( String picturePath )
    {
        return pictureEJB.create( picturePath );
    }

    public Image getImageTest()
    {
        Picture im = pictureEJB.getImageByName( "test" );
        return im.generateImage();
    }

    public String getPicturePath()
    {
        return picturePath;
    }

    public void setPicturePath( String picturePath )
    {
        this.picturePath = picturePath;
    }
}
