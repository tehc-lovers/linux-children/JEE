package be.helha.aemt.control;

import be.helha.aemt.dao.CourseDAO;
import be.helha.aemt.ejb.ActivityEJB;
import be.helha.aemt.ejb.EventEJB;
import be.helha.aemt.ejb.PictureEJB;
import be.helha.aemt.entities.Course;
import be.helha.aemt.entities.Event;
import be.helha.aemt.entities.Picture;
import org.primefaces.model.file.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class EventControl extends ActivityObservable implements Serializable
{
    @EJB
    private EventEJB EventEJB;
    @EJB
    private ActivityEJB activityEJB;
    @EJB
    private PictureEJB pictureEJB;
    //private Event event = new Event();
    private Integer id;
    private String name;
    private String place;
    private String description;
    private String type;
    private String frequence;
    private int maxTime;
    private int maxPlace;

    public int getMaxPlace() {
        return maxPlace;
    }
    public void setMaxPlace(int macPlace) {
        this.maxPlace = macPlace;
    }
    public String getFrequence() {
        return frequence;
    }
    public void setFrequence(String frequence) {
        this.frequence = frequence;
    }
    public int getMaxTime() {
        return maxTime;
    }
    public void setMaxTime(int val) {
        if(val >0)
            this.maxTime=val;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List< Event > showEventList()
    {
        return EventEJB.getAllEvents();
    }

    public String createNewEvent()
    {

        try
        {
            Course c;
            switch (type) {
                case "event":
                    EventEJB.createEvent(name, place, description, maxTime);
                    break;
                case "workshop":
                    c= new Course(name, place, maxPlace, maxTime, description, "Unique");
                    activityEJB.createCourse(c);
                    break;
                case "course":
                    c= new Course(name, place,maxPlace, maxTime, description, frequence);
                    activityEJB.createCourse(c);
                    break;
                default:
                	EventEJB.createEvent(name, place, description, maxTime);
                    break;
            }

        } catch ( Exception e )
        {
            e.printStackTrace();
        }
        return "/activity/activity_list.xhtml?faces-redirect=true";
    }

    public Event getEventById( int id )
    {
        return EventEJB.getEventById( id );
    }

    public EventEJB getEventEJB() {
        return EventEJB;
    }

    public void setEventEJB(EventEJB eventEJB) {
        EventEJB = eventEJB;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*public Event getEvent()
    {
        return event;
    }

    public void setEvent( Event event )
    {
        this.event = event;
    }*/
}
