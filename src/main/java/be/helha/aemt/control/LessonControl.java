package be.helha.aemt.control;

import be.helha.aemt.ejb.ActivityEJB;
import be.helha.aemt.ejb.LessonEJB;
import be.helha.aemt.entities.Activity;
import be.helha.aemt.entities.Lesson;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Named
@SessionScoped
public class LessonControl implements Serializable
{
    @EJB
    private LessonEJB lessonEJB;
    @EJB
    private ActivityEJB activityEJB;
    //private Lesson lesson = new Lesson();
    private Integer id;
    private Integer idActivity;
    private String name;
    private Date startingDateTime = GregorianCalendar.getInstance().getTime();
    private int duration;

    public String showCreateLessonPage()
    {
        return "/admin/lesson_create.xhtml?faces-redirect=true&id_activity=" + idActivity;
    }

    public List< Lesson > showLessonList()
    {
        return lessonEJB.getAllLessons();
    }

    public List< Lesson > showLessonListByActivity( int idActivity )
    {
        return lessonEJB.getAllLessonsByActivity( idActivity );
    }

    public String createNewLesson()
    {
        try
        {
            lessonEJB.createLesson( idActivity, name, startingDateTime, duration );
        } catch ( Exception e )
        {
            e.printStackTrace();
            return "/error/lesson_creation_error.xhtml?faces-redirect=true&id_activity=" +
                    idActivity +
                    "&date=" +
                    startingDateTime +
                    "&message=" +
                    e.getMessage();
        }
        return "/activity/activity_list.xhtml?faces-redirect=true";
    }

    public Lesson getLessonById( int id )
    {
        return lessonEJB.getLessonById( id );
    }

    public Lesson getLessonByName( String name )
    {
        return lessonEJB.getLessonByName( name );
    }

    public LessonEJB getLessonEJB()
    {
        return lessonEJB;
    }

    public void setLessonEJB( LessonEJB lessonEJB )
    {
        this.lessonEJB = lessonEJB;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Date getStartingDateTime()
    {
        return startingDateTime;
    }

    public void setStartingDateTime( Date startingDateTime )
    {
        this.startingDateTime = startingDateTime;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration( int duration )
    {
        this.duration = duration;
    }

    public Integer getIdActivity()
    {
        return idActivity;
    }

    public void setIdActivity( Integer idActivity )
    {
        this.idActivity = idActivity;
    }

    /*public Lesson getLesson()
    {
        return lesson;
    }

    public void setLesson( Lesson lesson )
    {
        this.lesson = lesson;
    }*/
}
