package be.helha.aemt.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class Activity implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String name;
    private String place;
    private String description;
    private int dureeTot;
    @ManyToMany( cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
    private List< Instructor > instructors;
    @OneToMany( cascade = CascadeType.REMOVE, fetch = FetchType.EAGER )
    private List< Lesson > lessons;
    private Picture picture;

    public Activity()
    {
        instructors = new ArrayList<>();
        lessons = new ArrayList<>();
    }

    public Activity( String name, String place, String description, int dureeTot )
    {
        this.name = name;
        this.place = place;
        this.dureeTot=dureeTot;
        this.description = description;
        instructors = new ArrayList<>();
        lessons = new ArrayList<>();
    }

    
    public int getDureeTot() {
		return dureeTot;
	}

	public void setDureeTot(int dureeTot) {
		this.dureeTot = dureeTot;
	}

	public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public List< Instructor > getInstructors()
    {
        List<Instructor> i= new ArrayList<Instructor>();
        for (Instructor instructor : this.instructors) {
			i.add(instructor);
		}
        return i;
    }

    public void setInstructors( List< Instructor > instructors )
    {
        this.instructors = instructors;
    }

    public String getPlace()
    {
        return place;
    }

    public void setPlace( String place )
    {
        this.place = place;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public List< Lesson > getLessons()
    {
        return lessons;
    }

    public void setLessons( List< Lesson > lessons )
    {
        this.lessons = lessons;
    }

    public Picture getPicture()
    {
        return picture;
    }

    public void setPicture( Picture picture )
    {
        this.picture = picture;
    }

    @Override
	public String toString() {
		return "Activity [id=" + id + ", name=" + name + ", place=" + place + ", description=" + description
				+ ", dureeTot=" + dureeTot + ", instructors=" + instructors + ", lessons=" + lessons + "]";
	}
    
    
}
