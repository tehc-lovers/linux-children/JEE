package be.helha.aemt.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Lesson
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String name;
    private LocalDateTime startingDateTime;
    private int duration;
    @ManyToMany( cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
    private List< Instructor > instructors;

    public Lesson()
    {
        instructors = new ArrayList<>();
    }

    public Lesson( String name, LocalDateTime startingDateTime, int duration )
    {
        this.name = name;
        this.startingDateTime = startingDateTime;
        this.duration = duration;
        instructors = new ArrayList<>();
    }

    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public LocalDateTime getStartingDateTime()
    {
        return startingDateTime;
    }

    public void setStartingDateTime( LocalDateTime startingDateTime )
    {
        this.startingDateTime = startingDateTime;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration( int duration )
    {
        this.duration = duration;
    }

    public List< Instructor > getInstructors()
    {
        return instructors;
    }

    public void setInstructors( List< Instructor > instructors )
    {
        this.instructors = instructors;
    }

    public Lesson clone()
    {
        Lesson tmpLesson = new Lesson( getName(), getStartingDateTime(), getDuration() );
        for( Instructor instructor : instructors )
        {
            tmpLesson.instructors.add( ( Instructor ) instructor.clone() );
        }
        return tmpLesson;
    }
}
