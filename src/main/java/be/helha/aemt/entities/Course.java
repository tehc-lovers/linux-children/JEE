package be.helha.aemt.entities;

import org.jline.builtins.Less;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Course extends ActivitySubscribable
{
    
    @ManyToMany( cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
    private List< Course > requirements;

    private String courseType;
    public Course()
    {
        requirements = new ArrayList<>();
    }

    public Course( String name,String place, int dureeTot,int nbplaces ,String description,String coursetype )
    {
        super(name,place, description,dureeTot , nbplaces);
        this.courseType = coursetype;
        requirements = new ArrayList<>();
    }


    public String getCourseType()
    {
        return courseType;
    }

    public void setCourseType( String courseType )
    {
        this.courseType = courseType;
    }

    public List< Course > getRequirements()
    {
        return requirements;
    }

    public void setRequirements( List< Course > requirements )
    {
        this.requirements = requirements;
    }

    @Override
    public Course clone()
    {
        Course tmpCourse = new Course(getName(), getPlace(), getDureeTot(), getPlacesMax(),getDescription(), this.courseType);
        for( Course course : requirements )
        {
            tmpCourse.requirements.add( course.clone() );
        }
        for( Lesson lesson : getLessons() )
        {
            tmpCourse.getLessons().add( lesson.clone() );
        }
        return tmpCourse;
    }

	@Override
	public String toString() {
		return super.toString() + "Course [requirements=" + requirements + ", courseType=" + courseType + "]";
	}
    
    

}
