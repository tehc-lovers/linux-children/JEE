package be.helha.aemt.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class User
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String login;
    private String password;

    @ManyToMany( cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
    private List< ActivitySubscribable > activities;

    public User()
    {
        activities = new ArrayList< ActivitySubscribable >();
    }

    public User( String login, String password )
    {
        activities = new ArrayList< ActivitySubscribable >();
        this.login = login;
        this.password = password;
    }

    private User( Integer id, String login, String password, List< ActivitySubscribable > activities )
    {
        this.activities = new ArrayList< ActivitySubscribable >();
        for ( ActivitySubscribable activitySubscribable : activities )
        {
            this.activities.add( activitySubscribable.clone() );
        }

        this.id = id;
        this.login = login;
        this.password = password;
    }

    public boolean subscribe( ActivitySubscribable i )
    {
        if ( !activities.contains( i ) )
        {
            activities.add( i );
            return i.subscribing( this );
        }
        return false;

    }

    public boolean unsubscribe( ActivitySubscribable i ) throws Exception
    {
        /*if ( activities.contains( i ) )
        {*/
        if ( !activities.remove( i ) )
        {
            throw new Exception();
        }
        return i.unsubscribing( this.clone() );
        /*}*/
        //return false;
    }

    public User clone()
    {
        return new User( this.id, this.login, this.password, this.activities );
    }

    public List< ActivitySubscribable > getActivities()
    {
        List< ActivitySubscribable > a = new ArrayList< ActivitySubscribable >();
        for ( ActivitySubscribable activity : activities )
        {
            a.add( activity.clone() );
        }
        return a;
    }


    @Override
    public int hashCode()
    {
        return Objects.hash( activities, id, login, password );
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        User other = ( User ) obj;
        return Objects.equals( getActivities(), other.getActivities() ) && Objects.equals( id, other.id )
                && Objects.equals( login, other.login ) && Objects.equals( password, other.password );
    }

    public Integer getId()
    {
        return id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin( String login )
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return "Utilisateur{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
