package be.helha.aemt.entities;

import javax.persistence.Entity;

@Entity
public class Event extends Activity
{
    public Event()
    {
    }

    public Event( String nom, String place, String description, int dureeTot )
    {
        super( nom, place, description, dureeTot );
    }

    public Event clone()
    {
        return new Event( getName(), getPlace(), getDescription() ,getDureeTot());
    }
}
