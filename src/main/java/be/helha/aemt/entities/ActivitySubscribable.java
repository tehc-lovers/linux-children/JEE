package be.helha.aemt.entities;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class ActivitySubscribable extends Activity implements ISubscribable
{
    @ManyToMany( cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
    private List< User > students;
    private int placesMax;

    public ActivitySubscribable()
    {
        students = new ArrayList<>();
    }

    public ActivitySubscribable( String name, String place, String description, int maxPlace, int dureeTot )
    {
        super( name, place, description, dureeTot );
        this.placesMax = maxPlace;
        students = new ArrayList<>();
    }


    public int getPlacesMax()
    {
        return placesMax;
    }

    public void setPlacesMax( int placesMax )
    {
        this.placesMax = placesMax;
    }

    @Override
    public boolean subscribing( User user )
    {
        //if(!students.contains(user))
        return students.add( user );
        //return false;
    }

    @Override
    public boolean unsubscribing( User user )
    {
        return students.remove( user );
    }

    @Override
    public String toString()
    {
        return super.toString() + "ActivitySubscribable [students=" + students + ", placesMax=" + placesMax + "]";
    }

    public abstract ActivitySubscribable clone();
}
