package be.helha.aemt.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.io.Serializable;

@Entity
public class Picture implements Serializable
{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String name;
    @Lob()
    private byte[] image;

    public Picture()
    {
    }

    public Picture( String name, byte[] image )
    {
        this.name = name;
        this.image = image;
    }

    public Image generateImage()
    {
        ImageIcon im = new ImageIcon( image );
        return im.getImage();
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Integer getId()
    {
        return id;
    }

    public byte[] getImage()
    {
        return image;
    }

    public void setImage( byte[] image )
    {
        this.image = image;
    }


}
